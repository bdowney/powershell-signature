# powershell-signature

## Certificate to sign scripts
You can generage a self-signed certificate or use an existing cert. It needs to be in Base64 and added to GitLab as an Environment Variable

Using a new self-signed certificate
```
$a = New-SelfSignedCertificate -Subject "My Code Signing Certificate” -Type CodeSigningCert -CertStoreLocation cert:\LocalMachine\My
$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
Export-PfxCertificate -Cert $a -FilePath .\mypfx.pfx -Password $mypwd
certutil -encode .\mypfx.pfx .\mypfx.txt
```

Using an existing certificate
```
$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
cd cert:\LocalMachine\My
dir -Recurse –CodeSigningCert
Export-PfxCertificate -Cert cert:\LocalMachine\My\0414FBE956D9A2259E1B2E8183B7BEB34E71F6F1 -FilePath .\mypfx.pfx -Password $mypwd
certutil -encode .\mypfx.pfx .\mypfx.txt
```


## Importing the certificate

```
certutil -decode $SIGNCERT .\mypfx.pfx
$cert = Import-PfxCertificate -FilePath .\mypfx.pfx -Password (ConvertTo-SecureString -String 1234 -AsPlainText -Force) -CertStoreLocation Cert:\LocalMachine\My
```


## Sign script
```
# $cert from import above or $cert = Get-PfxCertificate
Set-AuthenticodeSignature -FilePath C:\users\Administrator\Documents\my_posh_script.ps1 -Certificate $cert
```
